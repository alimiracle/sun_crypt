import 'package:flutter/material.dart';
import 'file_encryptor_screen.dart';
import 'about_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sun Crypt',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => FileEncryptorScreen(),
        '/about': (context) => const AboutScreen(), // Add this route for the About Screen
      },
    );
  }
}
