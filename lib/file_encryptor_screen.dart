import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'dart:io';
import 'file_helper.dart';
import 'about_screen.dart';

class FileEncryptorScreen extends StatefulWidget {
  @override
  _FileEncryptorScreenState createState() => _FileEncryptorScreenState();
}

class _FileEncryptorScreenState extends State<FileEncryptorScreen> {
  final _passwordController = TextEditingController();
  final FileHelper _fileHelper = FileHelper();

  Future<void> _encryptFile() async {
    if (_passwordController.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Password cannot be empty')),
      );
      return;
    }

    FilePickerResult? result = await FilePicker.platform.pickFiles();

    if (result != null) {
      File selectedFile = File(result.files.single.path!);
      String? selectedDirectory = await FilePicker.platform
          .getDirectoryPath(dialogTitle: "Select Directory To Save The File");

      if (selectedDirectory == null) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('No directory selected.')),
        );
        return;
      }

      File encryptedFile =
          await _fileHelper.fileEncrypt(selectedFile, _passwordController.text);
      String originalFileName = selectedFile.path.split('/').last;

      await FileHelper.saveToDirectory(
          encryptedFile, selectedDirectory, originalFileName + ".aes");

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('File encrypted successfully!')),
      );
    }
  }

  Future<void> _decryptFile() async {
    if (_passwordController.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Password cannot be empty')),
      );
      return;
    }

    FilePickerResult? result = await FilePicker.platform.pickFiles();

    if (result != null) {
      File selectedFile = File(result.files.single.path!);
      String? selectedDirectory = await FilePicker.platform
          .getDirectoryPath(dialogTitle: "Select Directory To Save The File");

      if (selectedDirectory == null) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('No directory selected.')),
        );
        return;
      }

      File decryptedFile =
          await _fileHelper.fileDecrypt(selectedFile, _passwordController.text);
      String originalFileName = selectedFile.path.split('/').last;
      originalFileName = originalFileName.replaceAll(".aes", "");
      await FileHelper.saveToDirectory(
          decryptedFile, selectedDirectory, originalFileName);

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('File decrypted successfully!')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sun crypt'),
        actions: [
          IconButton(
            icon: const Icon(Icons.info),
            onPressed: () {
              Navigator.pushNamed(context, '/about');
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _passwordController,
              decoration: InputDecoration(labelText: 'Password'),
              obscureText: true,
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _encryptFile,
              child: Text('Encrypt File'),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _decryptFile,
              child: Text('Decrypt File'),
            ),
          ],
        ),
      ),
    );
  }
}
