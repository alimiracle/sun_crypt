# Sun Crypt:

Sun Crypt is a cross-platform application that allows users to encrypt and decrypt files using a password. The app provides a simple interface for selecting files, entering a password, and saving the encrypted or decrypted files to a chosen directory.

## Features:

- **Encrypt Files:** Securely encrypt your files with a password.
- **Decrypt Files:** Decrypt previously encrypted files using the correct password.

## Getting Started:

### Build Prerequisites:

- Flutter SDK installed on your development environment.
- Android Studio or Xcode for running the app on an emulator or physical device.

### Installation:

1. Clone the repository to your local machine:

   ```bash
   git clone https://gitlab.com/alimiracle/sun_crypt
```

2. Navigate to the project directory:

```bash
cd sun_crypt
```

3. Run the following command to install dependencies:

```bash
flutter pub get
```

4. Run the app on an emulator or physical device:

```bash
flutter run
```

5. To build a release version of the app, run the following commands:

For Android (APK):

```bash
flutter build apk
```

For iOS (IPA):

```bash
flutter build ios
```

for windos:

```bash
flutter build windows
```

For Linux:

```bash
flutter build linux
```

For macOS:

```bash
flutter build macos
```

The built Files will be located in the `build` directory of your project, depending on the platform you targeted.


### Usage:

1. **Launch the App:** Start the app on your device or emulator.

2. **Enter Password:** Enter the password you wish to use for encryption or decryption.

3. **Encrypt a File:**
   - Tap the "Encrypt File" button.
   - Select the file you want to encrypt.
   - Choose the directory where you want to save the encrypted file.
   - The encrypted file will be saved with an `.aes` extension.

4. **Decrypt a File:**
   - Tap the "Decrypt File" button.
   - Select the file you want to decrypt (must have been encrypted by Sun Crypt).
   - Choose the directory where you want to save the decrypted file.
   - The decrypted file will be saved with its original name (without the `.aes` extension).

## Contributing:

We welcome contributions from the community! If you encounter any bugs, have feature requests, or would like to contribute enhancements, please follow these steps:

1. **Fork** the repository on GitHub.
2. **Clone** your forked repository to your local machine.
   ```bash
https://gitlab.com/alimiracle/sun_crypt
```

## Getting Help:

So you need help.

People can help you, but first help them help you, and don't waste their time.

Provide a complete description of the issue.

If it works on A but not on B and others have to ask you: "so what is different between A and B" you are wasting everyone's time.

"hello", "please" and "thanks" are not swear words.

## License:

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details.
